<?php

header('Content-Type:text/html;charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	$messages = array();
	if (!empty($_COOKIE['save'])) {
		setcookie('save', '', 100000);
		$messages[] = 'Спасибо, результаты сохранены.';
    }


	$flag=FALSE;
	$errors = array();
	$errors['name']=!empty($_COOKIE['name_error']);	
	$errors['email']=!empty($_COOKIE['email_error']);
	$errors['date'] = !empty($_COOKIE['date_error']);
	$errors['dzen1']=!empty($_COOKIE['dzen1_error']);
	$errors['dzen2']=!empty($_COOKIE['dzen2_error']);
	$errors['strenght']=!empty($_COOKIE['strenght_error']);
	$errors['biography']=!empty($_COOKIE['biography_error']);
	$errors['check1']=!empty($_COOKIE['check1_error']);
  

	if ($errors['name']) {
		setcookie('name_error', '', 100000);  //в скобках  (Название cookie,значение cookie,время)
		// Выводим сообщение.
		$messages[] = '<div class="error">Заполните имя корректно.</div>';
	  }
	
	 if ($errors['email']) {
		setcookie('email_error', '', 100000);
		$messages[] = '<div class="error">Корректно заполните email.</div>';
	  }
	
	  if ($errors['date']) {
		setcookie('date_error', '', 100000);
		$messages[] = '<div class="error">Поставьте год.</div>';
	  }
	
	 if ($errors['dzen1']) {
		setcookie('dzen1_error', '', 100000);
		$messages[] = '<div class="error">Укажите пол.</div>';
	  }
	
	 if ($errors['dzen2']) {
		setcookie('dezn2_error', '', 100000);
		$messages[] = '<div class="error">Укажите количество конечностей.</div>';
	  }
	
	 if ($errors['strenght']) {
		setcookie('strenght_error', '', 100000);
		$messages[] = '<div class="error">Укажите суперсилу.</div>';
	  }
	
	 if ($errors['biography']) {
		setcookie('biography_error', '', 100000);
		$messages[] = '<div class="error">Напишите биографию.</div>';
	  }
	
	 if ($errors['check1'])  {
		 setcookie('check1_error', '', 100000);
		 $messages[]= '<div class="error">Примите соглашение.</div>';
	 }
	

// Складываем предыдущие значения полей в массив, если есть.
$values = array();
//если $_COOKIE['name_value'] пустое,то значение будет '' 
//иначе $_COOKIE['name_value']
  $values['name']=empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email']=empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'] ;
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['dzen1']=empty($_COOKIE['dzen1_value']) ? '' : $_COOKIE['dzen1_value'];
  $values['dzen2']=empty($_COOKIE['dzen2_value']) ? '' : $_COOKIE['dzen2_value'];
  $values['strenght']=empty($_COOKIE['strenght_value']) ? '' : $_COOKIE['strenght_value'];
  $values['biography']=empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['check1']=empty($_COOKIE['check1_value'] ? '' : $_COOKIE['check1_value']);
  include('forma.php');
}
// Иначе, если запрос был методом POST
else {
	// Проверяем ошибки
	 $errors = FALSE;
   
	 if (empty($_POST['name']) || !preg_match("/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",$_POST["name"])) {
		// Выдаем куку на день с флажком об ошибке в поле name
	   setcookie('name_error', '1', time() + 24 * 60 * 60);
	   $errors = TRUE;
	 }
   
	 else {
	   setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
	 }
   
   
	 if (empty($_POST['email']) || !preg_match("/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",$_POST["email"])){
	   setcookie('email_error', '1', time() + 24*60*60);
	   $errors=TRUE;
	 }
   
	 else{
	   setcookie('email_value', $_POST['email'], time()+ 30*24*60*60);
	 }
   
   
	 if (empty($_POST['date'])){
	   setcookie('date_error', '1', time() + 24*60*60);
	   $errors=TRUE;
	 }
   
	 else{
	   setcookie('date_value', $_POST['date'], time()+ 30*24*60*60);
	 }
   
   
	 if (empty($_POST['dzen1'])){
	   setcookie('dzen1_error', '1', time() + 24*60*60);
	   $errors=TRUE;
	 }
   
	 else{
	   setcookie('dzen1_value', $_POST['dzen1'], time()+ 30*24*60*60);
	 }
   
   
	 if (empty($_POST['dzen2'])){
	   setcookie('dzen2_error', '1', time() + 24*60*60);
	   $errors=TRUE;
	 }
   
	 else{
	   setcookie('dzen2_value', $_POST['dzen2'], time()+ 30*24*60*60);
	 }
   
   
	if (empty($_POST['strenght'])){
		setcookie('strenght_error', '1', time() + 24*60*60);
		$errors=TRUE;
	  }
	   else{
	   $strenght_separated=implode(' , ',$_POST['strenght']);
	   setcookie('strenght_value', $strenght_separated, time()+ 30*24*60*60);
	   }
	   
   
	 if (empty($_POST['biography'])){
	   setcookie('biography_error', '1', time() + 24*60*60);
	   $errors=TRUE;
	 }
   
	 else{
	   setcookie('biography_value', $_POST['biography'], time()+ 30*24*60*60);
	 }
   
   if(empty($_POST['check1'])){
		   setcookie('check1_error', '1', time()+24*60*60);
		   $errors=TRUE;
	   }
   
	   else {
		   setcookie('check1_value', $_POST['check1'], time()+30*24*60*60);
	   }
	   
   
	 if ($errors) {
		// При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
	   header('Location: index.php');
	   exit();
	 }
	 else {
		// Удаляем Cookies с признаками ошибок.
		setcookie('name_error', '', 100000);
		setcookie('email_error', '', 100000);
		setcookie('date_error', '', 100000);
		setcookie('dzen1_error', '', 100000);
		setcookie('dzen2_error', '', 100000);
		setcookie('strenght_error', '', 100000);
		setcookie('biography_error', '', 100000);
		setcookie('check1_error', '', 100000);
	  }
	$user = 'u40989';
    $pass = '34623664';
    $db = new PDO('mysql:host=localhost;dbname=u40989', $user, $pass);

    $name = $_POST['name'];
    $email = $_POST['email'];
    $date = $_POST['date'];
    $gender = $_POST['dzen1'];
    $limbs = $_POST['dzen2'];
    $strenght_separated=implode(' , ',$_POST['strenght']); 
    $comment = $_POST['biography'];
	try
    {
        $stmt = $db->prepare("INSERT INTO application (name, email, date, dzen1, dzen2, strenght, biography) VALUES (:name, :email, :date, :dzen1, :dzen2, :strenght, :biography)");
        $stmt->bindParam(':name', $name);//Привязка переменных к параметрам подготавливаемого запроса 
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':dzen1', $dzen1);
        $stmt->bindParam(':dzen2', $dzen2);
        $stmt->bindParam(':strenght', $strenght_separated);
        $stmt->bindParam(':biography', $biography);
        $stmt->execute();
        print ('Результаты отпралены!');
        exit();
    }
	catch(PDOException $e)
    {
        print (': ' . $e->getMessage());
        exit();
    }
	setcookie('save', '1');
	// Делаем перенаправление.
   header('Location: index.php');
}
?>
